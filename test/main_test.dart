

import 'package:flutter_test/flutter_test.dart';
import 'package:localization_json/main.dart';

void main() {
  group('Home', () {
    test('Empty Email Test', () {
      var result = emailValidator('');
      expect(result, 'Email is required');
    });

    test('Invalid Email Test', () {
      var result = emailValidator('asdfasdfasdf');
      expect(result, 'Enter a valid Email');
    });

    test('Valid Email Test', () {
      var result = emailValidator('ajay.kumar@nonstopio.com');
      expect(result, null);
    });

    test('Empty Password Test', () {
      var result = passwordValidator('');
      expect(result, 'password is required');
    });

    test('Invalid Password Test', () {
      var result = passwordValidator('123');
      expect(result, 'password must be at least 8 digits long');
    });

    test('Valid Password Test', () {
      var result = passwordValidator('ajay12345');
      expect(result, "passwords must have at least one special character");
    });
  });
}
