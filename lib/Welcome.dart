import 'package:flutter/material.dart';
import 'package:localization_json/language_constants.dart';
import 'package:localization_json/main.dart';

class welcome extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(getTranslated(context, "welcome_page")),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(getTranslated(context, "welcome"),
                style: TextStyle(
                    fontWeight: FontWeight.bold,fontSize: 40.0)
                ,),
              Padding(padding: EdgeInsets.all(10.0)),
              Text(getTranslated(context, "user_logged_in"),
                style: TextStyle(
                    fontSize: 20.0)
                ,),


            ]),),


    );
  }
}
