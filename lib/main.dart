import 'package:flutter/material.dart';
import 'package:localization_json/DemoLocalizations.dart';
import 'package:localization_json/classes/language.dart';
import 'package:localization_json/Welcome.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:localization_json/language_constants.dart';
import 'package:form_field_validator/form_field_validator.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key key}) : super(key: key);

  static void setLocale(BuildContext context, Locale newLocale) {
    _MyAppState state = context.findAncestorStateOfType<
        _MyAppState>();
    state.setLocale(newLocale);
    final String title ="";
  }


  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {


  Locale _locale;
  setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  @override
  Widget build(BuildContext context) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        locale: _locale,
        supportedLocales: [
          const Locale('en','EN'),
          const Locale('hi', 'HI'),
          const Locale('ka','KA')
        ],

        localizationsDelegates: [
          DemoLocalization.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate
        ],
        localeResolutionCallback: (locale, supportedLocales) {
          for (var supportedLocale in supportedLocales) {
            if (supportedLocale.languageCode == locale.languageCode &&
                supportedLocale.countryCode == locale.countryCode) {
              return supportedLocale;
            }
          }
          return supportedLocales.first;
        },
          home: MyHomePage(),);
    }
  }



class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

final emailValidator = MultiValidator([
  RequiredValidator(errorText: 'Email is required'),
  PatternValidator(r'^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$', errorText: 'Enter a valid Email')]);

final passwordValidator = MultiValidator([
  RequiredValidator(errorText: 'password is required'),
  MinLengthValidator(8, errorText: 'password must be at least 8 digits long'),
  PatternValidator(r'(?=.*?[#?!@$%^&*-])', errorText: 'passwords must have at least one special character')]);


TextEditingController namecontroller = new TextEditingController();
TextEditingController passcontroller = new TextEditingController();

class _MyHomePageState extends State<MyHomePage> {
  final formKey = GlobalKey<FormState>();

  void _changeLanguage(Language language) async
  {
    Locale _locale = await setLocale(language.languageCode);
    MyApp.setLocale(context, _locale);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
          home: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              title: Text(getTranslated(context, 'login_page')),
              actions: [
                Padding(padding: EdgeInsets.all(16.0),
                  child: DropdownButton(
                      onChanged: (Language language) {
                        _changeLanguage(language);
                      },
                      underline: SizedBox(),
                      icon: Icon(
                        Icons.language,
                        color: Colors.white,
                      ),
                      items: Language.languageList()
                          .map<DropdownMenuItem<Language>>((lang) =>
                          DropdownMenuItem(
                              value: lang,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceAround,
                                children: [
                                  Text(lang.name, style: TextStyle(
                                      fontSize: 30
                                  ),), Text(lang.flag)
                                ],
                              ))).toList()
                  ),),
              ],
            ),
            body: Center(
                child: Form(
                  key: formKey,
                    child:Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("images/Cumulation.jpg",
                      height: 200.0,
                      width: 400.0,
                    ),
                    Padding(padding: EdgeInsets.all(10.0)),
                    SizedBox(
                      height: 50.0,
                      width: 250.0,
                      child: TextFormField(
                        controller: namecontroller,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(15.0),
                            hintText: getTranslated(context, 'Email'),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0),
                            )
                        ),
                          validator: emailValidator,
                      ),
                    ),
                    Padding(padding: EdgeInsets.all(5.0)),
                    SizedBox(
                      height: 50.0,
                      width: 250.0,
                      child: TextFormField(
                        controller: passcontroller,
                        obscureText: true,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(15.0),
                            hintText: getTranslated(context, 'password'),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0),
                            )
                        ),
                          validator: passwordValidator,
                      ),
                    ),
                    Padding(padding: EdgeInsets.all(10.0)),
                    Material(
                      elevation: 5.0,
                      borderRadius: BorderRadius.circular(30.0),
                      color: Colors.blue,
                      child: MaterialButton(
                          minWidth: 150,
                          child: Text(getTranslated(context, 'login'),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold
                            ),),
                          onPressed: () {
                            if(formKey.currentState.validate()){
                              formKey.currentState.save();
                            Navigator.push(context, MaterialPageRoute(
                                builder: (context) => welcome()));
                          }}


                      ),
                    ),
                  ],

                )),),
          )
    );
    }
  }


