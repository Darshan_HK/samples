import 'package:flutter/material.dart';
import 'package:localization_json/DemoLocalizations.dart';

const String LANGUAGE_CODE = 'languageCode';

//languages code
const String ENGLISH = 'en';
const String KANNADA = 'ka';
const String HINDI = 'hi';

Future<Locale> setLocale(String languageCode) async {
  return _locale(languageCode);
}

Future<Locale> getLocale(String languageCode) async {
  return _locale(languageCode);
}

Locale _locale(String languageCode) {
  switch (languageCode) {
    case ENGLISH:
      return Locale(ENGLISH, 'EN');
    case KANNADA:
      return Locale(KANNADA, "KA");
    case HINDI:
      return Locale(HINDI, "HI");
    default:
      return Locale(ENGLISH, 'EN');
  }
}

String getTranslated(BuildContext context, String key) {
  return DemoLocalization.of(context).translate(key);
}